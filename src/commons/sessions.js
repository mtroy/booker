import Sessions from "./sessions";
import {logger} from "./utils"

const log = logger()

let DB;
export default {
	init: () => 
	{
		navigator.storage.persist();
		const request = window.indexedDB.open("bookdirs", 1);


		log.db("init", request)
		request.onupgradeneeded = e =>
		{
			const db = request.result;


			log.db("create")
			if (!db.objectStoreNames.contains('bookdir'))
			{
				log.db("not contain bookie store")
				const store = db.createObjectStore("bookdir", {
				keyPath: "id"
				});

				store.createIndex("url", "url", { unique: false });
				store.createIndex("text", "text");
				// store.createIndex("path", "path");
				store.createIndex("icon", "icon");
				// store.createIndex("date", "date");
			}
		};

		return new Promise(resolve => 
		{
			request.onsuccess = e => 
			{
				DB = request.result;
				log.db("init SUCCESS")
				resolve(e);
			};
			request.onerror = e => 
			{
				log.db("init ERROR", request, e);
			};
		});
	},

	DBUpdate: async () =>
	{
		let sessions;
		try {
			sessions = await Session.getAll();
			await Session.deleteAll();
		} catch (e) {
			log.db("DBUpdate()", e)
			return;
		}

		for (let session of sessions) 
		{
			await Session.put(session).catch(e => {
				log.db("DBUpdate()", e)
			});
		}
	},

	update: async (op) =>
	{
		for (let session of op.update) 
		{
			await Sessions.put(session).catch(e => {
				log.db("update()", e)
			});
		}

		for (let session of op.delete) 
		{
			await Sessions.delete(session.id).catch(e => {
				log.db("delete()", e)
			});
		}

		return await Sessions.getAll();
	},

	put: session => 
	{
		const db = DB;
		const transaction = db.transaction("bookdir", "readwrite");
		const store       = transaction.objectStore("bookdir");
		const request     = store.put(session);

		return new Promise((resolve, reject) => 
		{
			request.onsuccess = () => {
				resolve();
			};
			request.onerror = e => {
				
				log.db("put()", e.target)
				reject(e.target);
			};
		});
	},

	delete: id => 
	{
		const db = DB;
		const transaction = db.transaction("bookdir", "readwrite");
		const store = transaction.objectStore("bookdir");
		const request = store.delete(id);

		return new Promise((resolve, reject) => 
		{
			transaction.oncomplete = () => {
				resolve();
			};
			transaction.onerror = e => {

				log.db("delete()", e.target)
				reject(e.target);
			};
		});
	},

	deleteAll: () => 
	{
		DB.close("sessions");

		const request = window.indexedDB.deleteDatabase("bookdirs");

		return new Promise(resolve => 
		{
			request.onsuccess = () => {
				resolve(Sessions.init());
			};
			request.onerror = e => {

				log.db("deleteAll()", e)
				reject(e);
			};
		});
	},

	get: id =>
	{
		const db = DB;
		const transaction = db.transaction("bookdirs", "readonly");
		const store = transaction.objectStore("bookdir");
		const request = store.get(id);

		return new Promise((resolve, reject) => 
		{
			request.onsuccess = () => 
			{
				if (request.result) 
				{
					resolve(request.result);
				} else reject(request);
			};
			request.onerror = e => {

				log.db("get()", e)
				reject(request);
			};
		});
	},

	getAll: async (needKeys = null) => 
	{
		const db = DB;
		const transaction = db.transaction("bookdir", "readonly");
		const store = transaction.objectStore("bookdir");
		const request = store.openCursor();

		let sessions = [];
		return new Promise((resolve, reject) => 
		{
			request.onsuccess = e => 
			{
				const cursor = request.result;
				if (cursor) 
				{
					let session = {};
					if (needKeys == null) 
					{
						session = cursor.value;
					} else {
						for (let i of needKeys) {
							session[i] = cursor.value[i];
						}
					}

					sessions.push(session);
					cursor.continue();

				} else {
					resolve(sessions);
				}
			};

			request.onerror = e => {

				log.db("getAll()", e)
				reject(request);
			};
		});
	},

	getAllWithStream: (sendResponse, needKeys, count) => 
	{
		const db = DB;
		const transaction = db.transaction("bookdirs", "readonly");
		const store = transaction.objectStore("bookdir");
		const request = store.openCursor();

		let sessions = [];

		request.onsuccess = e => 
		{
			const cursor = request.result;
			if (cursor) 
			{
				let session = {};
				if (needKeys == null)
				{
					session = cursor.value;
				} else 
				{
					for (let i of needKeys) 
					{
						session[i] = cursor.value[i];
					}
				}

				sessions.push(session);
				if (sessions.length === count) {
					sendResponse(sessions, false);
					sessions = [];
				}
				cursor.continue();
			} else {
				sendResponse(sessions, true);
			}
		};

		request.onerror = e => {
		};
	},

	search: (index, key) => 
	{
		const db = DB;
		const transaction = db.transaction("bookdir", "readonly");
		const store = transaction.objectStore("bookdir");
		const request = store.index(index).openCursor(key, "next");

		let sessions = [];
		return new Promise(resolve => 
		{
			request.onsuccess = e => {
				const cursor = request.result;
				if (cursor) {
					sessions.push(cursor.value);
					cursor.continue();
				} else {
					resolve(sessions);
				}
			};

			request.onerror = e => {

				log.db("search()", e)
				resolve();
			};
		});
	}
};
