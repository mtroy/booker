
export const formatFileSize = (bytes,decimalPoint) =>
{
   if(bytes == 0) return '0 Bytes';
   let k = 1000,
       dm = decimalPoint || 2,
       sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
       i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

export const logger = function (o)
{
	o = o || {
		db     : '#31a7cb',
		sidebar: '#3142cb',
		popup  : '#6c3bc2',
		bg     : '#C70E99',
		dev    : '#0bb65e',
		load   : '#cbac31',
		warn   : '#FF3F3F'
	};

	// console.log(new Error().stack.split('\n'))
	let script = new Error().stack.split('\n').map((e) =>
	{

    	const re = /^(?:@moz-extension).*\/scripts\/([\w]+\.(?:js)+\b)/g.exec(e)
    	// const re = /^(?:@webpack-internal:\/\/\/|setup@webpack-internal).*\.\/src(?:\/.*)?\/([\w]+\.(?:js|vue)+)\b/g.exec(e)
    	return (re?.[1]) || false
	}).filter(e => e)
	script = script.shift()

	const proto = (name, color, separator = '::') =>
	{
    	return (...args) =>
	    {
	    	name = name.toUpperCase()
	  		let messageConfig = `%c%s ${separator} `
	        args.forEach((argument) =>
	        {
			    const type = typeof argument
			    switch (type)
			    {
			        case 'bigint':
			        case 'number':
			        case 'boolean':
			            messageConfig += '%d   '
			            break

			        case 'string':
			            messageConfig += '%s   '
			            break

			        case 'object':
			        case 'undefined':
			        default:
			            messageConfig += '%o   '
		    	}
	        })
	        if(env === "development")
	        	console.log(messageConfig, 'color: ' + color, `[${name}](${script})`, ...args)
    	}
	}

	return Object.entries(o).reduce((a, [f, c]) =>
	{
    	a[f] = proto(f, c)
    	return a
	}, {})
}

