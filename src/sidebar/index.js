import browser from 'webextension-polyfill';
import {logger, formatFileSize} from '../commons/utils'
import Sessions from '../commons/sessions'
import './../styles/sidebar.css'

import {$tree} from './treeDom'
// import * as Builder from './treeBuilder'
import * as Do from './treeMethods';
import * as Events from './treeEvents'
import Messenger from './messenger'
// Object.entries(exports).forEach(([name, exported]) => window[name] = exported);


const log = logger()


// browser.tabs.sendMessage(this.state.tabId, {aim: 'erase'});
browser.runtime.onMessage.addListener(Messenger.Receive)



$tree.dom.jstree(
{
	"core" : 
	{
		"themes" : 
		{
			"responsive": false
		},
		"dots"          : false,
		// "open_parents"  : true,
		"check_callback": Do.checkCallback,
		'data'          : $tree.treeBuilt
	},
	'initially_open': ['/'],
	"checkbox":
	{
		keep_selected_style: true,
		visible            : true,
		three_state        : false, // to avoid that fact that checking a node also check others
		whole_node         : false, // to avoid checking the box just clicking the node 
		tie_selection      : false // for checking without selecting and selecting without checking
	},
	"types" : 
	{
		"root": { icon: "/media/img/root.png" },
		"default": { icon: "/media/img/fclosed.png" },
	},
	"state" : { "key" : "demo2", "three_state" : false },
	'search': {
		'case_insensitive' : true,
		'show_only_matches': true,
		"search_callback": Do.searchCallback
	},
	"plugins" : [ "dnd", "state", "types", "search", "changed", "conditionalselect", "contextmenu" ],

	"contextmenu":
	{
		items:
		{
			opt1:
			{
				separator_before: false,
				separator_after: true,
				label: "Add dir",
				action: (data) =>
				{
					Do.addNode()
				}, // submenu: { subopt0: {} }
			},
			opt2:
			{
				separator_before: false,
				separator_after: true,
				label: "Edit",
				action: (data) =>
				{
					let node = Do.getNode(data.reference[0])
					Do.editNode(node)

				}, // submenu: { subopt0: {} }
			},

		}
	}

})
.on("copy_node.jstree move_node.jstree", Events._onCopyMove)

.on('search.jstree', Events._onSearch)

.on('rename_node.jstree', Events._onRename)

.on("delete_node.jstree", Events._onDelete)

.on('show_contextmenu.jstree', Events._onContextMenu)

.on('keydown.jstree', Events._onKeyDown)
// .on("check_node.jstree uncheck_node.jstree", _onCheckAction)
// .on("changed.jstree", _onChange)

// tree.bind("hover_node.jstree", _onHoverNode)


//   ____ _____  ____  ____ _________  ____ _____  ________
//  / __ `/ __ \/ __ \/ __ `/ ___/ _ \/ __ `/ __ \/ ___/ _ \
// / /_/ / /_/ / /_/ / /_/ / /  /  __/ /_/ / / / / /__/  __/
// \__,_/ .___/ .___/\__,_/_/   \___/\__,_/_/ /_/\___/\___/
//     /_/   /_/


.on("select_node.jstree", Events._onNodeSelect)
.on('open_node.jstree', Events._nodeOpen)
.on('close_node.jstree', Events._nodeClose);

$tree.dom.bind("loaded.jstree", Events._evLoaded);
$tree.dom.bind("activate_node.jstree", Events._evClickNode);
$tree.dom.bind("refresh.jstree", Events._evRefresh);
$tree.dom.bind('create_node.jstree', Events._evCreateNode);
//                               __
//    ________  ____ ___________/ /_
//   / ___/ _ \/ __ `/ ___/ ___/ __ \
//  (__  )  __/ /_/ / /  / /__/ / / /
// /____/\___/\__,_/_/   \___/_/ /_/

$('#tree_search').keyup(function(){
    $tree.dom.jstree(true).show_all();
    $tree.dom.jstree('search', $(this).val());
});



//               __  _
//   ____ ______/ /_(_)___  ____  _____
//  / __ `/ ___/ __/ / __ \/ __ \/ ___/
// / /_/ / /__/ /_/ / /_/ / / / (__  )
// \__,_/\___/\__/_/\____/_/ /_/____/


$("#exp").click(function()
{
	let date = new Date().toISOString().replaceAll(':', '.')
	Do.exportJson(`bookerDump_${date}.json`)
})


$("#imp").on('change', function() { Do.importJson(this) });

$("#del").click(function() { Do.delNode() })

$("#opt").click(function() { $("#info").toggle() })

$("#sel").click(function() { })

$('#add').click(function () { Do.addNode() });

$("#tree").on("keydown", ".jstree-anchor", function(e)
{
	if(e.which === 13 || e.which === 32)
	{
		$tree.instance(true).toggle_node(e.target)
	}

	// if(e.ctrlKey && e.keyCode == 69)
	// {
	// 	log.dev("I've been pressed!");
	// }
});

window.addEventListener('beforeunload', (event) => 
{
	event.preventDefault();
	event.returnValue = '';

	log.sidebar("Tab is closing")
});


//     __    _           __   __
//    / /_  (_)___  ____/ /  / /_____  __  _______
//   / __ \/ / __ \/ __  /  / //_/ _ \/ / / / ___/
//  / /_/ / / / / / /_/ /  / ,< /  __/ /_/ (__  )
// /_.___/_/_/ /_/\__,_/  /_/|_|\___/\__, /____/
//                                  /____/

const bindKeys = (command)=>
{
	if(command === "book")
	{
		// log.dev("booking")
		browser.tabs.query({currentWindow: true, active: true}).then(function(tab)
		{
			Do.importNode(tab)
		}, console.error);
	}

	if(command === "fold")
	{
		// log.sidebar("COM: fold")
		Do.addNode()
	}
}

browser.commands.onCommand.addListener(bindKeys);


//     ____      _ __
//    /  _/___  (_) /_
//    / // __ \/ / __/
//  _/ // / / / / /_
// /___/_/ /_/_/\__/

const init = async () => {

	// const response = browser.runtime.sendMessage(null, { type: "load" }, null)
	Messenger.Send({ type: "load" }).then((response)=>
	{
		log.sidebar("loading tree", response)

		$tree.treeBase = response.db
		Do.generateTree($tree.treeBase)

		$tree.dom.bind("dblclick.jstree", Events._evEditNode);
	
		$tree.IsSideBarInit = true;
		log.sidebar("-- initialized --")
	})


}

init()
