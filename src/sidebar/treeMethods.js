import {logger, formatFileSize} from '../commons/utils'
import {v4 as uuidv4} from 'uuid';
import {$tree} from './treeDom'
import * as Builder from './treeBuilder'
import Messenger from './messenger'
import Tab from './tab'

const log = logger()

//    ______                                          __
//   / ____/___  ____ ___  ____ ___  ____ _____  ____/ /____
//  / /   / __ \/ __ `__ \/ __ `__ \/ __ `/ __ \/ __  / ___/
// / /___/ /_/ / / / / / / / / / / / /_/ / / / / /_/ (__  )
// \____/\____/_/ /_/ /_/_/ /_/ /_/\__,_/_/ /_/\__,_/____/


export const refresh = (callback = Function()) =>
{
	$tree.dom.jstree(true).refresh()
	if(typeof callback === "function")
		callback()	
}

export const generateTree = (data, callback) =>
{
	updateInfo(data)

	$tree.treeBuilt = Builder.buildTree(data)
	log.sidebar("built tree", $tree.treeBuilt)

	nearestFold()
	$tree.dom.jstree(true).settings.core.data = $tree.treeBuilt;
	
	refresh()
}

export const hideCheckboxes = (mode=undefined)=>
{
	switch(mode)
	{
		case "hide":
			$tree.dom.find('.jstree-checkbox').hide();
			$tree.dom.jstree(true).deselect_all();
			break;

		case undefined:
		case "toggle":
			$tree.dom.find('.jstree-checkbox').toggle(0, function()
			{

				$tree.dom.jstree(true)
				[
					$(this).is(':visible')
					? "select_all"
					: "deselect_all"
				]();
			});
		break;
	}
}

export const unSelectAll = () =>
{
	$tree.dom.jstree(true).deselect_all();
}

export const currentSelected = () =>
{
	// let nodes = Do.config.dom.jstree(true).get_selected()
	let nodes = $tree.dom.jstree('get_selected', true)
	return nodes;
}

export const currentAllSelected = () =>
{
	log.sidebar("all selected", $tree.dom.jstree(true).get_bottom_selected(true))
}

export const toggleCheckboxes = () =>
{
	$tree.dom.find('.jstree-checkbox').toggle(0, function()
	{
		$tree.dom.jstree(true)
		[
			$(this).is(':visible')
			? "select_all"
			: "deselect_all"
		]();
	});
}

export const getNestedChilds = (root) =>
{
	let n = []
	const traverse = (node) =>
	{
		if(root.id !== node.id)
			n.push(node);

		// Attempt to traverse if the node has children
		if($tree.dom.jstree(true).is_parent(node))
		{
			$.each(node.children, function(index, child)
			{
				let c = $tree.dom.jstree(true).get_node(child)
				traverse(c);
			});
		}
	};
	traverse(root)
	return n.filter(e=>e.data!==null)
}


export const updatePaths = (node) =>
{
	// let childs = tree.jstree(true).get_children_dom(node);
	let childs = getNestedChilds(node)
	if(childs.length)
	{
		for(let cnode of childs)
		{
			cnode.data.path = '/'+$tree.dom.jstree(true).get_path(cnode).slice(1, -1).join('/')
		}

	}else
	{
		log.warn("Has no childs", $tree.dom.jstree(true).get_node(node), childs)
	}
}

export const checkCallback = (op, node, parent, position, more) =>
{
	switch(op)
	{
		case "move_node":
		
			const destination = more.ref ?? undefined;
			if(more.ref !== undefined && !isFoldable(more.ref))
			{
				return false
			}
			break;

		case "delete_node":

			log.dev("callback delete", $tree.treeMassDelete)

			if(!isRoot(node))
				return $tree.treeMassDelete || confirm("Are you sure you want to delete?");
			return false
			break;

		default:
			return true
	}
}

export const searchCallback = function(str, node)
{
	$tree.treeSearchOpened = true
	//search for any of the words entered
	var word, words = [];
	var searchFor = str.toLowerCase().replace(/^\s+/g, '').replace(/\s+$/g, '');
	if (searchFor.indexOf(' ') >= 0)
	{
		words = searchFor.split(' ');
	} else
	{
		words = [searchFor];
	}

	for (var i = 0; i < words.length; i++)
	{
		word = words[i];
		if ((node.text || "").toLowerCase().indexOf(word) >= 0)
		{
			return true;
		}
	}
	return false;
}

export const isFoldable = (node) =>
{
	if(node===undefined) // folder
		return false
	return node.data === null || node.data?.path === undefined // folder
}

export const isRoot = (node) =>
{
	return node.id === "/"
}

export const saveTree = (mode) =>
{
	let save = Builder.flatTree($tree.dom.jstree(true).get_json('#', {flat:false}))

	$tree.treeBase = save
	Messenger.Send({ type: 'update', data:save })
	// browser.runtime.sendMessage(null, { type: 'update', data:save }, null);
	log.sidebar("Saving Tree", mode, save.map(e=>({text:e.text, path:e.data.path})))

	updateInfo(save)
}

export const updateInfo = (data) =>
{
	$tree.treeInfo.dbsize = JSON.stringify(data).length
	$tree.treeInfo.nbnodes = data.length
	$("#i_dbsize").text(formatFileSize($tree.treeInfo.dbsize))
	$("#i_nbnodes").text($tree.treeInfo.nbnodes)
}


export const importJson = function(that)
{
	let reader = new FileReader();
	reader.onload = function()
	{
		let arrayBuffer = JSON.parse(this.result)

		$tree.treeSaveImport = true
		// log.dev("import", data);
		$tree.treeBase = arrayBuffer
		generateTree(arrayBuffer, ()=>{})
	}
	reader.readAsText(that.files[0], 'UTF-8'); 
}

export const importNode = (data) =>
{
	log.dev("import node first", $tree.treeCurrentNode)

	let parent = currentNode()
	if(!isFoldable(parent))
	{
		parent = nearestFold()
	}

	log.dev("import node bis", '/'+$tree.dom.jstree(true).get_path(parent).slice(1).join('/'))
	log.dev("importe node", parent, $tree.treeLastOpened, $tree.treeCurrentNode, $tree.treeNearestFold)

	data = data.shift()

	const d = {
		icon: data.favIconUrl,
		text: data.title,
		a_attr: {href:data.url}, // exclusively for tab recognition
		data: 
		{
			url  : data.url,
			path : '/'+$tree.dom.jstree(true).get_path(parent).slice(1).join('/'),
			date : Date.now()
		},
		id: uuidv4()
	}

	createNode(parent, d, "last", function(n)
	{
		document.getElementById(d.id ).scrollIntoView()
		Tab.updateIcon("added")
		saveTree("new")	
	})

	// $tree.dom.jstree(true).create_node(parent, d, "last", function(n)
	// {
	// 	document.getElementById(d.id ).scrollIntoView()
	// 	saveTree("new")
	// })
}

export const exportJson = (filename, data=false) =>
{
	data = data || Builder.flatTree($tree.dom.jstree(true).get_json('#', {flat:false}))

	const blob = new Blob([JSON.stringify(data)], { type: "text/json" });
	const link = document.createElement("a");

	link.download            = filename;
	link.href                = window.URL.createObjectURL(blob);
	link.dataset.downloadurl = ["text/json", link.download, link.href].join(":");

	const evt = new MouseEvent("click", {
		view      : window,
		bubbles   : true,
		cancelable: true,
	});

	link.dispatchEvent(evt);
	link.remove()
};

export const createNode = (parent, data, pos, cb) =>
{
	$tree.dom.jstree(true).create_node(parent, data, pos, cb)	
}


export const addNode = () =>
{
	let node = currentNode()

	if(!isFoldable(node))
	{
		node = nearestFold()
	}

	createNode(node.id, {text:"new block", type:"default"}, "last", function(n)
	{
		$tree.dom.jstree(true).deselect_all();

		selectNode(n.id)
		openNode(n.id)
		this.edit(n);

	})
	// $tree.dom.jstree("create_node", node.id, {text:"new block", type:"default"}, "last", function (n)
	// {
	// 	$tree.dom.jstree(true).deselect_all();

	// 	selectNode(n.id)
	// 	openNode(n.id)
	// 	this.edit(n);
	// });
}

export const delNode = () =>
{
	let node = currentNode()
	let nodes = currentSelected()

	if(nodes.length > 1)
	{		
		if(confirm("Are you sure you want to delete all selected nodes ?"))
		{
			nearestFold() // preserve current nearest folder to avoid empty it after deletion

			log.dev("before delete", node, nodes)
			$tree.treeMassDelete = true
			for(let n of nodes)
			{
				$tree.dom.jstree(true).delete_node(n)
			}
			$tree.treeMassDelete = false
		}
	}else
	{
		nearestFold() // preserve current nearest folder to avoid empty it after deletion
		$tree.dom.jstree(true).delete_node(node)
	}

	Tab.updateIcon("remove", node)
	saveTree("remove")
}

export const getParent = (node) =>
{
	return $tree.dom.jstree(true).get_parent(node)
}

export const getNode = (data) =>
{
	data = data !== undefined 
		? data 
		: '/'

	log.dev("GETNODE", data)
	// let id = data !== undefined
	// 	? typeof data === "string"
	// 		? data
	// 		: data.reference[0] // from event
	// 	: '/'
	return $tree.dom.jstree(true).get_node(data)
}

export const editNode = (node) =>
{
	$tree.dom.jstree(true).edit(node);
}

export const openNode = (id) =>
{
	$tree.dom.jstree("open_node", id)
}

export const siblingsNode = (node) =>
{
	return getNode(node).children_d
	// return tree.jstree(true).get_node(node).children_d;
}

export const selectNode = (id) =>
{
	$tree.dom.jstree(true).select_node(id)
	$tree.treeCurrentNode = id
	log.dev("select node", id)
	return getNode(id)
}

export const currentNode = () =>
{
	$tree.treeCurrentNode = $tree.dom.jstree('get_selected', true)[0]?.id ?? '/'
	log.dev("currentNode()", $tree.treeCurrentNode)
	return selectNode($tree.treeCurrentNode)
}


export const nearestFold = () =>
{
	let node = selectNode(currentNode())
	if(typeof node !== "undefined")
	{
		if(!isFoldable(node))
		{
			log.dev("near not foldable", node)
			$tree.treeNearestFold = getParent(node)
		}else
		{
			log.dev("near foldable", node)
			$tree.treeNearestFold = node.id
		}
	}else
	{
		$tree.treeNearestFold = '/'
	}

	return selectNode($tree.treeNearestFold)
}
