import {logger, formatFileSize} from '../commons/utils'
import {$tree} from './treeDom'

const log = logger()

//     ____        __
//    / __ \____ _/ /_____ _
//   / / / / __ `/ __/ __ `/
//  / /_/ / /_/ / /_/ /_/ /
// /_____/\__,_/\__/\__,_/


export const getData = () =>
{

}

export const getBranch = (a, path) => 
{
    return path.reduce((a, item) => 
    {
        if(typeof a.children === "undefined")
          a.children = []

        var access = a.children.find(x=>x.text===item)
        if(access === undefined)
            a.children.push((access = {text: item, type:"default", children:[]})) // make directory (item is like /first/second/third/..)

        return access
    }, a);
}

export const buildTree = (base) =>
{
	return [base.reduce((a,b)=>
	{
		b.a_attr = {href: b.data.url}
	    var bp = b.data.path.split('/').filter(e=>e) // get path array
	    if(bp.length > 0)
	    {
	    	// create folder before add link
	        var atmp = getBranch(a, bp) // the new folder
	        
	        // console.log("getbranch", b)
	        atmp.children.push(b) // add link
	    }else
	    {
	    	// link
	        a.children.push(b)
	    }
	    return a;
	}, {
	    text:"/", type: "root", id:"/", state:{opened:true}, children:[]
	})]
}


export const flatTree = (array) => array.flatMap(({text, data, icon, id, children}) => 
{
	return [
		{ text, data,icon, id }, ...flatTree(children || [])
	];

}).filter(x=>(x.data!==undefined && x.data.path!==undefined));

