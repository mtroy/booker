import {logger, formatFileSize} from '../commons/utils'
import * as Do from './treeMethods';


export default{
	Receive: (({ type, data }, sender, responder) => 
	{	
		switch (type)
		{
			case "new": // add bookmark from popup button

				Do.importNode(data)
				return "new"
		}
	}),


	Send: (data) =>
	{
		return browser.runtime.sendMessage(null, data, null);
	}
}
