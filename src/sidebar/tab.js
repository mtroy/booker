import {logger, formatFileSize} from '../commons/utils'
import {$tree} from './treeDom'

const log = logger()

export default
{
	updateIcon: (mode, node) =>
	{
		browser.tabs.query({currentWindow: true, active: true}).then(function(tab)
		{
			switch(mode)
			{
				case "remove":
					
					let m = $tree.treeBase.find(e=>e.data.url===node.data.url)
					log.dev(node.data.url, $tree.treeBase, m)
					if(m === undefined)
					{
						browser.browserAction.setIcon({path: '/media/img/bookmark.png', tabId: tab[0].id});
					}
					break;

				case "added":
					browser.browserAction.setIcon({path: '/media/img/bookmarked.png', tabId: tab[0].id});
					break;
			}
			
		}, console.error);
	}
}
