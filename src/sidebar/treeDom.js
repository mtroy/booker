export let $tree = 
{
	IsSideBarInit   : false,
	treeBuilt       : [{text:"/", id:'/', children:[]}],
	treeBase        : undefined,
	treeNearestFold : undefined,
	treeCurrentNode : undefined,
	treeLastOpened  : '/',
	/* states */
	treeSaveImport  : false,
	treeMassDelete  : false,
	treeSearchOpened: false,
	/* info */
	treeInfo        : {dbsize:0, nbnodes:0},
	// .on("dnd_stop.vakata", function(data, el, help, ev)
	// {
	//     console.log("dropped", data)
	// })
	dom             : $("#tree"),
	instance        : $("#tree")["jstree"]
}
