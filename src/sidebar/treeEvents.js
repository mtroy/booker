import {logger, formatFileSize} from '../commons/utils'
import {$tree} from './treeDom'
import * as Builder from './treeBuilder'
import * as Do from './treeMethods';

const log = logger()

//     ______                 __
//    / ____/   _____  ____  / /______
//   / __/ | | / / _ \/ __ \/ __/ ___/
//  / /___ | |/ /  __/ / / / /_(__  )
// /_____/ |___/\___/_/ /_/\__/____/


export const _onChange = (e, data) =>
{
	console.log("changed node", data.instance.get_path(data.node))
}

export const _onCheckAction = (e, data) =>
{
	testit(); // first line of this function does not get any selected data, even if several are selected. When called from a button click event in my form it does work. 
}

export const _onCopyMove = (e, data) =>
{
	// update paths of moved nodes, before saving

	if(Do.isFoldable(data.node)) // from parent
	{
		Do.updatePaths(data.node)

	}else // from bookmark
	{
		data.node.data.path = '/'+data.instance.get_path(data.node).slice(1, -1).join('/')
	}

	Do.saveTree("move")
}

export const _nodeOpen = (evt, data) =>
{
	$tree.treeLastOpened = data.node
	// log.dev("lastopen", $tree.treeLastOpened, data.node.id)
	if(data.node.data === null && !Do.isRoot(data.node))
	{
		data.instance.set_icon(data.node, '/media/img/fopen.png');
	}
}

export const _nodeClose = (evt, data) =>
{
	if(data.node.data === null && !Do.isRoot(data.node))
	{
		data.instance.set_icon(data.node, "/media/img/fclosed.png");
	}
}

export const _onNodeSelect = (evt, data) =>
{
	let node = Do.currentNode()

	if($tree.treeSearchOpened)	
	{
		$tree.dom.jstree(true).clear_search()
		$tree.treeSearchOpened = false

		let toOpen = node
		if(!Do.isFoldable(node))
		{
			Do.selectNode(node)
			toOpen = Do.nearestFold()
			$tree.dom.jstree(true).open_node(toOpen)
		}
		
		$tree.dom.jstree(true)._open_to(toOpen)
		document.getElementById(toOpen.id ).scrollIntoView()
		log.dev("from clearing", toOpen)
		$("#tree_search").val('')
	}
}

export const _onHoverNode = (e, data) =>
{
	let node = data.node
}

export const _onSearch = (nodes, str, res) => 
{
	if (str.nodes.length===0) {
		$tree.dom.jstree(true).hide_all();
	}
}

export const _onContextMenu = (e, ref, element) =>
{
	let node = ref.node
	log.dev("context", node)

	// log.dev(node.data)
	// if (reference.node.parents.length != 1)
	// {
	// 	$.vakata.context.hide();
	// 	//$('.vakata-context').css('display',"none");
	// }
}

export const _onKeyDown = (e) =>
{
	if(e.which === 46)
	{
		e.preventDefault();

		Do.delNode()
	}
}

export const _onDelete = (e, data) =>
{
	log.dev("deletion")
}

export const _onRename = (e, data) =>
{
	Do.updatePaths(data.node)

	log.dev("REN", data.node.id)

	Do.saveTree("rename") 
}

//     ____  _           __
//    / __ )(_)___  ____/ /____
//   / __  / / __ \/ __  / ___/
//  / /_/ / / / / / /_/ (__  )
// /_____/_/_/ /_/\__,_/____/


export const _evLoaded = (event, data) =>
{
	// To hide root nodes text
	$("a:contains('/')").css("visibility","hidden")  

	// Need to find other way to hide rootNodeName Any1 knows ?
	log.sidebar("hiding root")

	// To hide - icon jstree-ocl
	$(".jstree-last .jstree-icon").first().hide()
}

export const _evClickNode = (e, data) =>
{
	if(!Do.isRoot(data.node))
	{
		data.instance.toggle_node(data.node)
	}
}
export const _evRefresh = (event, data) =>
{
	if($tree.treeSaveImport)
	{
		$tree.treeSaveImport = false
		Do.saveTree("import")
	}
}

export const _evCreateNode = (event, data) =>
{

	if(!Do.isFoldable(data.node))
	{
		log.dev("at create", $tree.treeCurrentNode.id, data.parent)
		$tree.dom.jstree(true).open_node(data.parent)
	}
}

export const _evEditNode = (event) =>
{
	let node = $tree.dom.jstree(true).get_node(event.target);
	if(node.data === null && !Do.isRoot(node))
	{
		$tree.dom.jstree(true).edit(node);
	}else
	{
		window.open(node.data.url, "_blank" )
	}
}

