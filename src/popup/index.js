import browser from 'webextension-polyfill';
import {logger} from '../commons/utils'



const log = logger()

/**
 * Sends message to background script as soon as this is clicked
 */

const addTab = async () => {

	await browser.tabs.query({currentWindow: true, active: true}).then(function(tab)
	{
		browser.runtime.sendMessage(null, { type: 'new', data: tab}, null);

	}, console.error);
};



log.popup("from popup")
addTab();
