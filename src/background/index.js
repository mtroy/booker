import browser from 'webextension-polyfill';
import {logger} from '../commons/utils'
import Sessions from '../commons/sessions'
import {v4 as uuidv4} from 'uuid';


const log = logger()




// var clicks = 0;
// function increment() {
// 	console.log(clicks)
//   browser.browserAction.setBadgeText({ text: (++clicks).toString() });
// }


async function exists(url) { return await Sessions.search("url", url) }

async function fetchAll(url) { return await Sessions.getAll() }

const isBookmarked = () =>
{

}


const diffRecord = (data) =>
{
	// log.dev(data, tmpBase)

	let from = tmpBase || []
	let to   = data

	var changed = to.filter(t => from.find(f => (t.id === f.id && (t.text !== f.text || t.data.path !== f.data.path) )))
	var added   = to.filter(t => !from.find(f => (t.id === f.id )))
	var removed = from.filter(f => !to.find(t => (f.id === t.id)))


	log.dev(from, to)
	log.dev("updatable", [...added, ...changed])
	log.dev("removable", removed)

	return {
		update:[...added, ...changed], 
		delete:removed
	}
}



//    __________  __  ___
//   / ____/ __ \/  |/  /
//  / /   / / / / /|_/ /
// / /___/ /_/ / /  / /
// \____/\____/_/  /_/


// Listen for tab activation and update oTabs
// to show if page has already been bookmarked


const updateIcon = (info, changed) => 
{
	// log.bg("TAB UPDATE", info)
	browser.tabs.query({currentWindow: true, active: true}).then(function(tab)
	{
		// exists(currTab.url).then((st) => { if(!st.length) {}}
		if(tmpBase !== undefined)
		{
			// log.bg("TAB", tab[0])
			let m = tmpBase.find(e=>e.data.url===tab[0].url)
			if(m !== undefined)
			{
				browser.browserAction.setIcon({path: '/media/img/bookmarked.png', tabId: tab[0].id});
			}else
				browser.browserAction.setIcon({path: '/media/img/bookmark.png', tabId: info.tabId});
		}		
	}, console.error);

	// browser.tabs.get(info.tabId).then((currTab) => 

}
browser.tabs.onUpdated.addListener(updateIcon)
browser.tabs.onActivated.addListener(updateIcon)



browser.runtime.onMessage.addListener(({ type, data }, sender, responder) => 
{
	switch (type)
	{
		case "load": // sender: sidebar

			log.bg("MSG", "Loading db")
			return new Promise((resolve, reject) => 
			{
				fetchAll().then((d) =>
				{
					log.bg("getALL", d)

					tmpBase = d
					resolve({db: d})
				})
			})
			
			return "load";

		case "update":

			let operations = diffRecord(data)
			return new Promise((resolve, reject) => 
			{
				Sessions.update(operations).then((d) =>
				{
					log.bg("update bookmarks", data)
					log.bg("getALL", d)

					tmpBase = d
					resolve({db: d})
				})
			})

			return "updateSave";

		// case 'INIT':
		// 	log.bg(data)
		// 	return 'initialized';

		default:
			return 'invalid action';
	}
});


let IsInit = false;
let tmpBase

const init = async () => 
{
	if (!('indexedDB' in window))
	{
		log.warn("This browser doesn't support IndexedDB");
		return;

	}else
	{
		await Sessions.init();
		IsInit = true;

		log.bg("-- initialized --")
	}
}

init();