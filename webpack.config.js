const path = require('path');
const MiniExtractCssPlugin = require('mini-css-extract-plugin');
const { DefinePlugin } = require('webpack');
//const HtmlBundlerPlugin = require('html-bundler-webpack-plugin');
//const CopyPlugin = require("copy-webpack-plugin");

module.exports = ({ mode }) => {
  const beforeEntry = [];

  if (mode === 'production') {
    beforeEntry.push('@babel/polyfill');
  }

  return {
    resolve: {
      extensions: ['.js', '.css', '.scss'],
    },

    plugins: [
		new MiniExtractCssPlugin({
			filename: './../media/css/[name].css',
		}),

		new DefinePlugin({
			env: JSON.stringify(mode)
		}),
		// new CopyPlugin({
		// 	patterns: 
		// 	[
		// 		{
		// 			from: "src/styles/sidebar.css",
		// 			to: "./../media/css/",
		// 		},
		// 	],
		// }),

    ],

    module:
    {
		rules:
		[
			{ test: /.js$/, exclude: /(node_modules)/, loader: 'babel-loader' },
			{
				test: /.s?css$/i,
				use: [MiniExtractCssPlugin.loader, 'css-loader', 'postcss-loader'],
				//use: [MiniExtractCssPlugin.loader, 'css-loader', 'sass-loader'],
			},
/*        {
            test: /\.css$/i,
            include: path.resolve(__dirname, 'src'),
            use: ['style-loader', 'css-loader', 'postcss-loader'],
            exclude: /\.module\.css$/,
        }*/
		],
    },

	entry:
	{
		background    : beforeEntry.concat('./src/background/index.js'),
		//service_worker: beforeEntry.concat('./src/worker/index.js'),
		sidebar       : beforeEntry.concat('./src/sidebar/index.js'),
		content       : beforeEntry.concat('./src/content/index.js'),
		popup         : beforeEntry.concat('./src/popup/index.js'),
	},

	output: {
		path: path.resolve('addon', 'scripts'),
		filename: '[name].js',
    },

    mode,
  };
};
